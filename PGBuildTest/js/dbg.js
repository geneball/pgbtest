// Author: Gene Ball   Aug 2012

// ToDo list:
//TODO
var DBG = {// debugging message control
    Debug : true,
    InTitle : false,
    WithTimes : true,
    SysVersion : "",
    BaseTime : 0,
    addVersion : function(s) {
        if (DBG.SysVersion == "") {
            DBG.SysVersion = DBG.Version;
            DBG.addExtensions();
        }
        if (DBG.SysVersion.indexOf(s) < 0)
            DBG.SysVersion = s.trim() + ' / ' + DBG.SysVersion;
        $('#jeb-Version').html(DBG.SysVersion);
    },
    dbg : function(cd, msg) {
        if (!DBG.Debug)
            return;
        var s = DBG.WithTimes ? DBG.gTime() : 'dbg> ';
        var fch = cd.charAt(0);
        if (fch == '-' || fch == '+') {
            cd = cd.substr(1);
            DBG.setDbg(cd, fch == '+');
        } else
            fch = '';
        s += cd + ': ' + msg;
        DBG.dbgLog.push({
            flag : cd,
            msg : s
        });
        if (cd == 'alert')
            window.alert(s);
        if (DBG.getDbg(cd)) {
            if (DBG.InTitle)
                document.title = s;
            if (window.console)
                window.console.log(s);
        }
        DBG.refresh();
    },
    gTime : function() {
        var tm = new Date().getTime();
        if (DBG.BaseTime == 0)
            DBG.BaseTime = tm;
        return (tm - DBG.BaseTime) + '> ';
    },
    dbgMax : 25,
    refresh : function() {
        var log = "";
        var cnt = 1;
        for (var i = DBG.dbgLog.length - 1; i >= 0; i--) {
            if (DBG.getDbg(DBG.dbgLog[i].flag)){
            	if (cnt++ > DBG.dbgMax) break;
                log += DBG.dbgLog[i].msg + "<br>";
            }
        }
        if (DBG.dbgInput != '')
            log += '<br> >>> ' + DBG.dbgInput;
        $("#jeb-Debug").html(log);
    },
    getDbg : function(cd) {
        if (DBG.dbgEnable[cd] == undefined)
            DBG.dbgEnable[cd] = true;
        return DBG.dbgEnable[cd];
    },
    setDbg : function(cd, enable) {
    	if (cd=='*'){
    		for (var i in DBG.dbgEnable)
    		  DBG.dbgEnable[i] = enable;
    	}
    	else
        	DBG.dbgEnable[cd] = enable;
        DBG.dbg('msg', 'flag {0} set to {1}'.format(cd, enable));
    },
    dbgMenu : ['Debug', 1, 'Show;d-hide;t/Title;x/X-exit;max;a/Activate flag;flags;?'],
    dbgInput : '',
    dbgKey : function(k) {
        var cmdsWithArgs = 'aAm';
        if (k == '\r') {
            DBG.dbgCmd(DBG.dbgInput);
            DBG.dbgInput = '';
        } else if (DBG.dbgInput.length == 0 && cmdsWithArgs.indexOf(k) < 0) {
            DBG.dbgCmd(k);
        } else {
            DBG.dbgInput += k;
            DBG.refresh();
        }
    },
    dbgCmd : function(cmd) {
    	DBG.dbg('dbg', 'dbgCmd('+cmd+')');
    	var fch = cmd.charAt(0);
    	if (fch=='+' || fch=='-'){ //  +/-flag => f+/-flag
    		cmd == 'f'+cmd;
    		fch = 'f';
    	}
    	var sch = cmd.length>1? cmd.charAt(1):'-';
    	var rst = cmd.length>2? cmd.substr(2).trim() : '';
        switch (fch) {
            case 's': /* S+/- =>  Show/don't debugging panel */
           		if (sch=='+')
                	$('#jeb-Debug').show('slow');
                else
                	$('#jeb-Debug').hide('fast');
                break;
            case 't':  /* T+/- => show/don't dbg output in Title */
                DBG.InTitle = (sch == '+');
                DBG.dbg('msg', 'show last debug message in Title set to {0}'.format(DBG.InTitle));
                break;
            case 'x':  /* X+/- => enable/disable debugging */
                DBG.Debug = (sch=='+');
                DBG.dbg('msg', 'debugging = ' + DBG.Debug.toString());
                break;
            case 'm':  /* m+/-xx => max of 'xx' debugging lines */
                DBG.dbgMax = float.parse(rst);
                break;
            case 'f':  /* f+/-nm => activate/disactivate 'nm' */
           		if (rst!='')
                	DBG.setDbg(rst, sch == '+');
                DBG.dbg('msg', JSON.stringify(DBG.dbgEnable));
                break;
            default:
                DBG.dbg('msg', 'command "{0}" unrecognized'.format(cmd));
            case '?':
                DBG.dbg('msg', '? help,S+/-showpanel,T+/-intitle,X+/-debugging,f+/-flag,m cnt maxlines');
                break;
        }
    },
    dbgLog : [],
    dbgEnable : {
        alert : true
    },
    addExtensions : function() {
        if (String.prototype.format === undefined) {
            String.prototype.format = function() {
                var formatted = this;
                for (var i = 0; i < arguments.length; i++) {
                    var regexp = new RegExp('\\{' + i + '\\}', 'gi');
                    formatted = formatted.replace(regexp, arguments[i]);
                }
                return formatted;
            };
        }
        if (String.prototype.startsWith === undefined) {
            String.prototype.startsWith = function(s) {
                return this.indexOf(s) == 0;
            };
            String.prototype.endsWith = function(s) {
                return this.indexOf(s) == (this.length - s.length);
            };
        }
    }
}
DBG.Version = "Dbg.10MayA"; 
// 10May dbgCmd lcase
// 9May  dbgCmd f+/-syntax,  setDbg('*')
// 8May  dbgCmd changes, show most recent first
// 22Feb fix dbgMax, '-dbg'
// 21Feb show only last dbgMax lines
// 16Feb default InTitle false
// 12Feb split from JEB, move addVersion, add timestamps
