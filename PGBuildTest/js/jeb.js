// Author: Gene Ball   Aug 2012

// ToDo list:
//TODO
var APP = {
    // variables based on runtime situation:
    //   App or Website?  location.href.startsWith('file:')
    IsApp : false,
    //   Platform?  == App? window.device.platform : 'web';  (web, iPhone, Android, WinPhone)
    Platform : 'web',
    //   Development or Production?  JEB.Version.includes('Development')
    IsDevel : false,
    //   only if App-- iOs, Android, etc
    Device : '?',
    DeviceVersion : '?',
    Domain : 'winenotes.biz',
    ResourcePath : '',
    AjaxPath : '',
    AppName : '',
    AppVersion : '',
    AppDir : '/app',
    IsPortrait : null,
    InnerWidth : 0,
    InnerHeight : 0,
    Scale : 4,
    Metric : false,
    ClosestTarget : 'n/a',
    LastBarcode : '',
    LastPush : '',
    PushID: ''
}
var JEB = {// JEB web app utilities
    onAppReady : null,
    onOrientation : null,
    onConfig : null,
    onDistanceToTarget : null,
    onPush : null,
    DevReady : false,
    targets : [{
        name : 'CurrentLocation',
        Lat : 0,
        Lng : 0,
        notifyAt : 0,
        metersAway : 1000
    }, {
        name : 'VillaDuVin',
        Lat : 47.73807,
        Lng : -122.16172,
        notifyAt : 0.1,
        metersAway : 1000
    }],
    onTargetChange : null,
    onChange : null, //called whenever something changes
    GeoTStamp : 0,
    pushNotification : null,
    barcodeScanner : null,

    init : function() {// set up device & DOM ready handlers
        DBG.dbg('init', 'jeb inline script');
        document.addEventListener('deviceready', JEB.onDeviceReady, false);
        $(document).ready(JEB.onDOMReady);
        DBG.addVersion(JEB.Version);
    },
    onDOMReady : function() {// setup environment, then call onAppReady
        DBG.dbg('init', 'JEB.onDOMReady()');
        JEB.readConfig();
        APP.IsDevel = DBG.SysVersion.indexOf('Development') > 0;
        if (APP.IsDevel) {
            $('#jeb-Version').show();
        }
        APP.InnerWidth = window.innerWidth;
        APP.InnerHeight = window.innerHeight;
        APP.IsPortrait = window.innerHeight > window.innerWidth;
        var url = location.href.toLowerCase();
        APP.ResourcePath = url.substr(0, url.lastIndexOf('/') + 1);
        if (url.startsWith('http')) {// also https
            APP.IsApp = false;
            APP.Platform = 'web';
            APP.Device = window.navigator.appName;
            APP.DeviceVersion = window.navigator.appVersion;
            url = url.substr(url.indexOf('//') + 2);
            APP.Domain = url.substring(0, url.indexOf('/'));
            APP.AjaxPath = APP.ResourcePath + 'server/ajax.asp';
            if (APP.IsDevel && !APP.ResourcePath.endsWith('dev/'))
                DBG.dbg('WARNING', 'Development code not running from dev path');
        } else {
            APP.IsApp = true;
            if (APP.IsDevel) {
                $('#jeb-Cmd').show();
                $('#jeb-PGEvents').show();
                $('#jeb-Debug').show();
            }
            APP.AjaxPath = 'http://' + APP.Domain + APP.AppDir + (APP.IsDevel ? 'dev' : '') + 'Server/ajax.asp';
        }
        DBG.dbg('ajax', 'path='+APP.AjaxPath);
        $('#jeb-Cmd').show();
        DBG.dbg('init', 'App={0} onAppReady is {1}'.format(APP.IsApp, APP.onAppReady == null ? 'null' : 'not null'));
        if (!APP.IsApp && JEB.onAppReady != null) {
            JEB.onAppReady();  // start web app
        } else
            window.setTimeout(JEB.checkMain, 5000);
    },
    checkMain : function() {// in case onDeviceReady doesn't happen
        DBG.dbg('init', 'JEB.checkMain()');
        if (JEB.DevReady)
            return;
        DBG.dbg('init', 'DevReady={0}, onAppReady is {1}'.format(JEB.DevReady, JEB.onAppReady == null ? 'null' : 'not null'));
        if (JEB.onAppReady != null)
            JEB.onAppReady();
        else
            DBG.dbg('init', 'still no onAppReady');
    },
    onDeviceReady : function() {// setup App
        JEB.DevReady = true;
        APP.Platform = window.device.platform;
        APP.Device = window.device.name;
        APP.DeviceVersion = window.device.version;

        DBG.addVersion('Cordova.' + APP.Platform);
        DBG.dbg("devReady", DBG.Version);

        if (window.plugins) {
            JEB.pushNotification = window.plugins.pushNotification;
            if (APP.Platform.toLowerCase() == 'android') {
                DBG.dbg('push', 'register GCM');
                var gcm = {
                    "senderID" : "55487087057",
                    "ecb" : "JEB.onNotificationGCM"
                };
                JEB.pushNotification.register(function(res) {
                    DBG.dbg('push', 'GCM registered ' + res)
                }, function(err) {
                    DBG.dbg('push', 'GCM registration failed ' + err);
                }, gcm);
            }
            if (APP.Platform.toLowerCase() == 'ios') {
                DBG.dbg('push', 'register APN');
                var apn = {
                    "badge" : "true",
                    "sound" : "true",
                    "alert" : "true",
                    "ecb" : "JEB.onNotificationAPN"
                };
                JEB.pushNotification.register(function(res) {
                    DBG.dbg('push', 'APN registered: pushid= ' + res.substr(0,20));
                    APP.PushID = res;
                    JEB.updateJSON({
                    	DB: APP.AppName,
                        operation : 'registerIOS',
                        app : APP
                    }, function(res) {
                        DBG.dbg('push', 'iOS pushID got to server');
                    });
                }, function(err) {
                    DBG.dbg('push', 'APN registration failed ' + err);
                }, apn);
            }
            JEB.barcodeScanner = window.plugins.barcodeScanner;
        }
        var phoneGapEvents = ["menubutton", "backbutton", "searchbutton", "pause", "resume", "online", "offline", "batterycritical", "batterylow", "batterystatus", "startcallbutton", "endcallbutton", "volumedownbutton", "volumeupbutton"];
        for (var i in phoneGapEvents) {
            $(document).on(phoneGapEvents[i], JEB.onPGEvent);
        }
        // if (navigator && navigator.network)
        // navigator.network.isReachable("google.com", JEB.reachableCallback, {});
        if (JEB.onAppReady != null)
            JEB.onAppReady();
        else
            window.setTimeout(JEB.checkMain, 5000);
    },
    onNotificationGCM : function(event) {
    	DBG.dbg('push', 'onNotificationGCM: '+event.event);
        APP.LastPush = 'got GCM push: ' + event.event;
        if (event.event == 'registered') {
            APP.LastPush += ' id=' + event.regid.substr(0,20);
            APP.PushID = event.regid;
            JEB.updateJSON({
            	DB : APP.AppName,
                operation : 'registerAndroid',
                app : APP
            }, function(res) {
                DBG.dbg('push', 'Android pushID got to server');
            });
        }
        if (event.event == 'message') {
            APP.LastPush += event.foreground ? ' while running ' : ' in bar ';
            APP.LastPush += event.payload.message + ' cnt=' + event.payload.msgcnt;
        }
        if (event.event == 'error') {
            APP.LastPush += ' ' + event.msg;
        }
        DBG.dbg("push", APP.LastPush);
        JEB.userCallback(JEB.onPush);
    },
    onNotificationAPN : function(event) {
        APP.LastPush = 'got APN push-- alert:{0} sound:{1} badge:{2}'.format(event.alert, event.sound, event.badge);
        if (event.badge)
            JEB.pushNotification.setApplicationBadgeNumber(function(res) {
                DBG.dbg("push", 'set Badge ' + res);
            }, event.badge);
        DBG.dbg("push", APP.LastPush);
        JEB.userCallback(JEB.onPush);
    },
    setOnClick : function(selector, filter, fn) {
        $(selector).on('touchstart click', filter, $.debounce(250, true, fn));
    },
    userCallback : function(oncall) {
        if (oncall && oncall != null)
            oncall();
        if (JEB.onChange != null)
            JEB.onChange();
    },
    readConfig : function() {
        DBG.dbg('jeb', 'readConfig');
        $.get("config.xml", {}, function(xml) {
            DBG.dbg('jeb', 'got config.xml');
            APP.AppName = $('name', xml).text();
            APP.AppVersion = $('widget', xml).attr('version');
            JEB.userCallback(JEB.onConfig);
        });
    },
    getApp : function(level) {
        level = level || 0;
        var app = '{0} V{1}'.format(APP.AppName, APP.AppVersion);
        if (level == 0)
            return app;
        app += ' from ' + APP.Domain;
        if (level == 1)
            return app;
        return app + ' (' + DBG.SysVersion + ')';
    },
    getPlatform : function(level) {
        level = level || 0;
        var pl = '{0} on {1}'.format(APP.Platform, APP.Device);
        if (level == 0)
            return pl;
        pl += ' {0}{1}x{2}'.format(APP.IsPortrait ? 'P' : 'L', APP.InnerWidth, APP.InnerHeight);
        if (level == 1)
            return pl;
        return pl + ' (' + APP.DeviceVersion + ')';
    },
    getVersion : function(level) {
        DBG.dbg('jeb', 'getVersion(' + level + ')');
        return JEB.getApp(level) + ' ' + JEB.getPlatform(level);
    },
    updateJSON : function(op, fn) {
        DBG.dbg('ajax', 'req ' + op.operation);
        $.ajax(APP.AjaxPath, {
            type : 'GET',
            data : { data: JSON.stringify(op) },
            dataType : 'json',
            success : function(data, textStatus, jqXHR) {
                DBG.dbg('ajax', op.operation + ' success');
                fn(data);
            },
            error : function(jqXHR, textStatus, errorThrown) {
                DBG.dbg('ajax', 'Error: ' + textStatus + ' XHR: ' + jqXHR.statusText + '<br>' + jqXHR.responseText);
            },
        });
    },
    trackTargets : function(targets, onTargetChange) {
        JEB.onTargetChange = (onTargetChange || null);
        JEB.targets = targets;
        if (targets == null || targets.length == 0)
            return;
        if (!navigator || !navigator.geolocation)
            return DBG.dbg("geo", "no navigator.geolocation");
        JEB.watchID = navigator.geolocation.watchPosition(JEB.checkTargets, JEB.onGeoError, {
            enableHighAccuracy : true
        });
    },
    checkTargets : function(position) {
        DBG.dbg("geo", 'Lat:{0} Lng:{1} Alt:{2} Acc:{3} Hd:{4} Spd: {5}'.format(position.coords.latitude, position.coords.longitude, position.coords.altitude, position.coords.accuracy, position.coords.heading, position.coords.speed, position.timestamp));
        if (JEB.GeoTStamp == 0) {// first time
            JEB.GeoTStamp = position.timestamp;
            JEB.targets[0].Lat = position.coords.latitude;
            JEB.targets[0].Lng = position.coords.longitude;
        }
        JEB.lastDelay = position.timestamp - JEB.GeoTStamp;
        JEB.GeoTStamp = position.timestamp;
        JEB.lastAcc = position.coords.accuracy;
        var iClose = 1;
        for (var i = 0; i < JEB.targets.length; i++) {
            var tg = JEB.targets[i];
            tg.metersAway = JEB.getDistanceFromLatLonInMeters(tg.Lat, tg.Lng, position.coords.latitude, position.coords.longitude);
            if (i > 0 && tg.metersAway < JEB.targets[iClose].metersAway)
                iClose = i;
        }
        JEB.iLastClose = iClose;
        JEB.targets[0].Lat = position.coords.latitude;
        JEB.targets[0].Lng = position.coords.longitude;
        JEB.updateLoc();
        JEB.userCallback(JEB.onTargetChange);
    },
    updateLoc : function() {
        var loc = JEB.targets[0];
        APP.Location = JEB.locStr(loc.Lat, loc.Lng) + ' +/- ' + JEB.distStr(JEB.lastAcc);
        if (loc.metersAway > 1)
            APP.Location += ' moved {0} in {1}'.format(JEB.distStr(loc.metersAway), JEB.durationStr(JEB.lastDelay));
        var close = JEB.targets[JEB.iLastClose];
        if (JEB.lastAcc > 10000 && close.area)
            APP.ClosestTarget = close.area + ' area';
        else if (close.metersAway < JEB.lastAcc)
            APP.ClosestTarget = 'within accuracy of ' + close.name;
        else
            APP.ClosestTarget = '{0} is {1} from here'.format(close.name, JEB.distStr(close.metersAway));
        for (var i in JEB.targets)
        JEB.targets[i].distance = JEB.distStr(JEB.targets[i].metersAway);
    },
    durationStr : function(msec) {
        if (msec < 1000)
            return sprintf('%3.0fmsec', msec);
        if (msec < 60000)
            return sprintf('%2.1fsec', msec / 1000);
        if (msec < 3600000)
            return sprintf('%2.1fmin', msec / 60000);
        return sprintf('%2.1fhr', msec / 3600000);
    },
    locStr : function(lat, lng) {
        var latstr = lat < 0 ? sprintf('%5.4fS', -lat) : sprintf('%5.4fN', lat);
        var lngstr = lng < 0 ? sprintf('%5.4fW', -lng) : sprintf('%5.4fE', lng);
        return latstr + ', ' + lngstr;
    },
    distStr : function(d) {
        if (APP.Metric) {
            if (d < 100)
                return sprintf('%2.1fm', d);
            var km = d / 1000;
            if (km < 500)
                return sprintf('%2.1fkm', km);
            if (km < 1000)
                return sprintf('%2.0fkm', km);
            var th = Math.floor(km / 1000);
            return sprintf('%d,%03.0fkm', th, km - th * 1000);
        }
        var ft = d * 3.28084;
        if (ft < 528)
            return sprintf('%2.0fft', ft);
        var mi = ft / 5280;
        if (mi < 100)
            return sprintf('%2.1fmi', mi);
        if (mi < 1000)
            return sprintf('%2.0fmi', mi);
        var th = Math.floor(mi / 1000);
        return sprintf('%d,%03.0fmi', th, mi - th * 1000);
    },
    onGeoError : function(error) {
        APP.Location = 'unknown: error {0} {1}'.format(error.code, error.message);
        DBG.dbg("geo", APP.Location);
        JEB.userCallback(JEB.onTargetChange);
    },
    getDistanceFromLatLonInMeters : function(lat1, lon1, lat2, lon2) {
        var R = 6378100/* Radius of the earth in m */;
        var dLat = JEB.deg2rad(lat2 - lat1);
        var dLon = JEB.deg2rad(lon2 - lon1);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(JEB.deg2rad(lat1)) * Math.cos(JEB.deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c/* Distance in m */;
        return d;
    },
    deg2rad : function(deg) {
        return deg * (Math.PI / 180)
    },
    onBarcodeClick : function(event) {
        DBG.dbg('jeb', 'onBarcodeClick()');
        if (JEB.barcodeScanner == null)
            return;
        var nm = event.target.innerText.toLowerCase();
        DBG.dbg('jeb', 'onBarcodeClick(' + nm + ')');
        if (nm == 'scan')
            scan();
        else {
            encode($('#jeb-barcode').attr('value'));
        }
        userCallback('');
    },
    onShowDbgClick : function(event) {
        if ($('#jeb-Debug').is(':visible'))
        	$('#jeb-Debug').hide();
        else
        	$('#jeb-Debug').show();
        DBG.dbg('jeb', 'onShowDebugClick()');
    },
    scan : function() {
        if (JEB.barcodeScanner == null)
            return;
        JEB.barcodeScanner.scan(function(result) {
            APP.LastBarcode = result.cancelled ? 'cancelled' : result.type + ' ' + result.data;
            DBG.dbg('jeb', 'scanWin ' + APP.LastBarcode);
        }, function(err) {
            APP.LastBarcode = 'failed ' + err;
            DBG.dbg('jeb', 'scanFail ' + APP.LastBarcode);
        });
    },
    encode : function(text) {
        if (JEB.barcodeScanner == null)
            return;
        text = text || "http://lootowl.com/";
        JEB.barcodeScanner.encode(BarcodeScanner.Encode.TEXT_TYPE, text, function(success) {
            APP.LastBarcode = 'Encoded: ' + success;
            DBG.dbg('jeb', 'encodeWin ' + APP.LastBarcode);
        }, function(err) {
            APP.LastBarcode = 'Encode failed ' + err;
            DBG.dbg('jeb', 'encodeFail ' + APP.LastBarcode);
        });
    },
    onResize : function(ev) {// std resize handler, updates IsPortrait, InnerSize
        APP.InnerWidth = window.innerWidth;
        APP.InnerHeight = window.innerHeight;
        var portrait = (window.innerHeight > window.innerWidth);
        var diff = (portrait != APP.IsPortrait);
        APP.IsPortrait = portrait;
        if (diff) {
            DBG.dbg('resize', 'switch to ' + ( portrait ? 'Portrait ' : 'Landscape ') + APP.InnerWidth + 'Wx' + APP.InnerHeight + 'H');
            JEB.userCallback(JEB.onOrientation);
        }
    },
    onPGEvent : function(ev) {// default debugging commands, catch backbutton
        DBG.dbg("pgevent", ev.type);
        $('#jeb-PGEvents').html($('#jeb-PGEvents').html() + ' ' + ev.type.toString());
        switch(ev.type.toString()) {
            case 'menubutton':
                $('#jeb-Debug').show();
                break;
            case 'backbutton':
                if (window.confirm('Exit? ')) {
                    if (JEB.pushNotification != null)
                        JEB.pushNotification.unregister(function() {
                        }, function() {
                        });
                    navigator.app.exitApp();
                }
                ev.preventDefault();
                break;
            case 'searchbutton':
                $('#jeb-Events').show();
                break;
            case 'volumeupbutton':
                $('#jeb-Version').show();
                break;
            default:
                break;
        }
    },
    onKeypress : function(event) {// default debugging commands
        event.preventDefault();
        var ch = String.fromCharCode(event.which);
        DBG.dbg('-events', 'keypress {0} = "{1}"'.format(event.which, ch))
        if ($('#jeb-Debug').is(':visible')) {
            DBG.dbgKey(ch);
            return;
        }
        JEB.onCmd(ch);
    },
    onCmd : function(cmd) {
        DBG.dbg('jeb', 'cmd(' + cmd + ')');
        switch (cmd.charAt(0)) {
            case 'E':
                $('#jeb-PGEvents').show();
                break;
            case 'V':
                $('#jeb-Version').show();
                break;
            case 'D':
                $('#jeb-Debug').show();
                if (cmd.length>1) DBG.dbgCmd(cmd.substr(1));
                break;
            case 'C':
                JEB.readConfig();
                break;
            default:
                DBG.dbgCmd(cmd);
                break;
        }
    },
    onInfoClick : function(event) {// hide infoBox when clicked
        event.stopPropagation();
        event.preventDefault();
        $(this).hide();
    },
    onAdjScale : function(event) {//adjust base font size
        $('body').toggleClass('jebScale' + APP.Scale);
        var sz = APP.Scale + (event.srcElement.id == 'jeb-Smaller' ? -1 : 1);
        APP.Scale = (sz < 1) ? 1 : (sz > 7 ? 7 : sz);
        $('body').toggleClass('jebScale' + APP.Scale);
    },
    onAdjUnits : function(event) {//toggle units
        APP.Metric = !APP.Metric;
        $('#jeb-UnitsInUse').text(APP.Metric ? 'Metric' : 'English');
        JEB.updateLoc();
        JEB.userCallback();
    },
    onPgClick : function(event) {// go to page named in link
        event.stopPropagation();
        var nm = event.target.innerText.toLowerCase();
        var pgid = '#' + nm + '-page';
        $('.jebPage').hide();
        $(pgid).show();
    },
    objToDiv : function(obj, fmt, divclass) {
        divclass = divclass || '';
        var html = divclass == '' ? '<div>' : sprintf("<div class='%s'>", divclass);
        if ($.isArray(obj)) {
            for (var i = 0; i < obj.length; i++)
                html += JEB.objToDiv(obj[i], fmt, 'item' + i);
        } else if ($.isPlainObject(obj)) {
            if (fmt) {
                html += sprintf(fmt, obj)
            } else {
                for (var i in obj) {
                    html += '<div> {0}: {1} </div>'.format(i, obj[i].toString());
                }
            }
        } else
            html += obj.toString();
        return html + '</div>';
    }
}
JEB.Version = "JEB.10MayD" /* check JEB.? */;
// 10May fix onShowDebug
//  9May ajax.asp
//  8May onCmd
//  7May add GCM: AppName to pushRegistration
// 21Feb pushNotification
// 20Feb barcode, debounce
// 18Feb list, dbg
// 17Feb use sprintf
// 16Feb updateJSON, resize, APP
// 15Feb config.xml & fontSize
// 13Feb onDevReady, default onPGEvent & onKeypress
// 12Feb split out DBG, rebuild minimal version
// 7Feb onDevReady
// 6Feb app/web config, dbgKey, split DBG
// 5Feb put App/Web initialization here
JEB.init()/* setup handlers */;
