// Author:  Gene Ball  Feb 2013 

// ToDo list:
//TODO  16Feb getJSON
var PGBT = {
    currDrill: 'V',
	targets: [
	{name: 'Last change', Lat : 0, Lng : 0, notifyAt : 0, metersAway : -1 },
	{name: 'VillaDuVin', Lat:47.73807, Lng:-122.16172, notifyAt: 0.1, area: 'Woodinville, WA'},
	{name: 'Pondera', Lat:47.769648, Lng:-122.149553, notifyAt: 0.2, area: 'Woodinville, WA' },
	{name: 'Efeste', Lat:47.77133, Lng:-122.14562, notifyAt: 0.2,  area: 'Woodinville, WA' },
	{name: 'Gold Creek', Lat:47.74042, Lng:-122.14572, notifyAt: 0.2, area: 'Woodinville, WA' },
	{name: 'Airfield Estates', Lat:47.73214, Lng:-122.140586, notifyAt: 0.2, area: 'Woodinville, WA' },
	{name: "Teddy's", Lat:47.75650, Lng:-122.15341, notifyAt: 0.2,  area: 'Woodinville, WA' },
	{name: 'MGM HQ', Lat:44.92571, Lng:-92.9659, notifyAt: 1, area: 'St Paul, MN' },
	{name: "Jack's", Lat:44.92159, Lng:-92.86587, notifyAt: 0.1, area: 'St Paul, MN' },
	{name: 'New Level Radio', Lat:39.74237, Lng:-104.95409, notifyAt: 1, area: 'Denver, CO' },
	{name: "Nathan's", Lat:39.75822, Lng:-104.92489, notifyAt: 1, area: 'Denver, CO' },
	{name: "Margie's", Lat:39.75844, Lng:-104.88523, notifyAt: 1, area: 'Denver, CO' },
	{name: 'MGM Grand Vegas', Lat:36.1025, Lng:-115.1702, notifyAt: 5, area: 'Las Vegas, NV' }
	 ],
    load : function() {
        APP.AppDir = '/pgbt'/*path inside web site: lootowl.com/pgbt/ */;
        JEB.onAppReady = PGBT.pgbtMain;
        APP.Domain = 'lootowl.com'
        DBG.addVersion($('#pgbTestVersion').css('font-family'));
        DBG.addVersion($('#pgbTestVersion').html());
        DBG.addVersion(PGBT.Version);
        DBG.dbg('init', 'pgbtest.js loaded');
    },
    pgbtMain : function() {
        DBG.dbg('pgbt', 'pgbtMain() ' + DBG.SysVersion);
        JEB.trackTargets(PGBT.targets)
        $('#home-page').show();
        PGBT.refresh();
        JEB.onChange = PGBT.refresh;  //whenever something changes
        
	    $(window).resize( $.throttle(250, false, JEB.onResize) );
	    JEB.setOnClick('.infoBox', '', JEB.onInfoClick) /*hide infoBox when clicked */;
        JEB.setOnClick('.pgClick', '', JEB.onPgClick) /* switch to page when clicked */;
        if (APP.IsApp)
	        $('#jeb-Cmd').on('submit', '', PGBT.onCmd);  /* handle debug cmd input on Apps */
        else
	        $(document).on('keypress', '', JEB.onKeypress) /*default handler with debugging cmds*/;
        JEB.setOnClick('.jebAdjScale', 'a', JEB.onAdjScale) /* adj font Size */;
        JEB.setOnClick('.jebAdjUnits', 'a', JEB.onAdjUnits) /* switch units */;
        JEB.setOnClick('.jebBarcode', 'a', JEB.onBarcodeClick) /* scan/encode barcode */;
        JEB.setOnClick('.jebShowDbg', 'a', JEB.onShowDbgClick) /* show barcode */;
        
        JEB.setOnClick('.drillClick', 'div', PGBT.onDrill) /* show more detail */;
        JEB.setOnClick('.listClick', 'div div', PGBT.listClick) /* toggle selected item */;
    },
    refresh: function(){
        $('#version').html(JEB.getApp(0));
        $('#app').html(JEB.getApp(1));
        $('#platform').html(JEB.getPlatform(1));
        $('#target').html(APP.ClosestTarget);
        $('#location').html(APP.Location);
        $('#barcode').html(APP.LastBarcode);
        $('#push').html(APP.LastPush);
        PGBT.reDrill();
    },
    onCmd: function(event){
        event.preventDefault();
    	var cmd = $('#jeb-cmdinput').val();
    	$('#jeb-cmdinput').val('');
    	JEB.onCmd(cmd); 
    },
    onDrill: function(event){
        event.stopPropagation();
    	PGBT.currDrill = $(event.target).text().trim();
    	PGBT.refresh();
    },
    reDrill: function(){
    	$('#details').html('');
    	switch (PGBT.currDrill.charAt(0)){
    	    case 'V': $('#details').html( 'Version<br>'+JEB.getVersion(2)); break;  
            case 'A': $('#details').html( 'App<br>'+JEB.objToDiv(APP)); break;  
            case 'P': $('#details').html( 'Platform<br>'+JEB.getPlatform(2)); break;  
            case 'L': $('#details').html( 'Location<br>'); break; 
            case 'T': $('#details').html( 'Targets<br>'+JEB.objToDiv(PGBT.targets, '%(name)s: %(distance)s')); break; //targets
            case 'B': $('#details').html( 'Barcode<br>'); break;
            case 'N': $('#details').html( 'Notification<br>'+APP.LastPush+'<br>'+APP.PushID); break;
    	}
    },
    onConfig: function(){ //called once when config.xml is read
    	DBG.dbg('pgbt', 'showConfig('+APP.AppName+')');
        $('#version').html(JEB.getVersion(0));
        $('#app').html(JEB.getApp(1));
    },
    listClick: function(event){
        DBG.dbg('list','listClick '+$(event.target).text());
    	event.stopPropagation();
    	$(event.target).toggleClass('sel');
    }
}
PGBT.Version = "PGBT-Development.10MayA"   /* check PGBT.?  */;
//  7May  test push registration
// 25Apr  back to pushNotification
// 21Feb  pushNotification
// 20Feb  barcodes, debounce
// 18Feb  onDrill
// 17Feb  targets
// 16Feb  objToDiv
// 12Feb  jeb created
PGBT.load()   /* setup pgbtMain to be called after everything loads (App or not) */;
